import { Pinecone } from "@pinecone-database/pinecone";
import { convertToAscii } from "./utils";
import { getEmbeddings } from "./embedding";

export async function getMatchesFromEmbedding( embeddings:number[], fileKey:string){
    const pinecone = new Pinecone({
        apiKey: process.env.PINECONE_API_KEY!,
        environment: process.env.PINECONE_ENVIRONMENT!,
    });
    const index = await pinecone.Index('chatpdf')

    try {
        const namespace= convertToAscii(fileKey)
        const queryResult= await index.query({
            vector: embeddings,
            filter: { filekey: { $eq: namespace } },
            topK: 5,
            includeMetadata: true
        })
        return queryResult.matches || []
    } catch (error) {
        console.log('error querying embeddings', error)
        throw(error)
    }
}


export async function getContext(query: string, fileKey:string){
    const queryEmbddings = await getEmbeddings(query)
    const matches = await getMatchesFromEmbedding(queryEmbddings,fileKey);

    const qualifyigDocs = matches.filter((match)=> match.score && match.score>0.7);

    type Metadata ={
        text:string,
        pageNumber: number
    }

    let docs=qualifyigDocs.map(match=> (match.metadata as Metadata).text)
    return docs.join('\n').substring(0,3000);


}