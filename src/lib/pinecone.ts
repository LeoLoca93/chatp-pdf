import { Pinecone, PineconeClient, PineconeRecord, utils as PineconeUtils} from "@pinecone-database/pinecone"
import { downloadFromS3 } from "./s3-server";
import { PDFLoader } from "langchain/document_loaders/fs/pdf"
import { Document, RecursiveCharacterTextSplitter } from "@pinecone-database/doc-splitter"
import { getEmbeddings } from "./embedding";
import { convertToAscii } from "./utils";
import md5 from 'md5';
import { FileKey } from "aws-sdk/clients/kinesisanalytics";
import { metadata } from "@/app/layout";


let pinecone: Pinecone | null = null;

export const getPineconeClient = async () => {

        pinecone = new Pinecone({
            apiKey: process.env.PINECONE_API_KEY!,
            environment: process.env.PINECONE_ENVIRONMENT!,
        }
        );
        return pinecone;

};

type PDFPage = {
    pageContent: string;
    metadata: {
        loc: { pageNumber: number };
        filekey: string
    }
}


export async function loadS3IntoPinecone(fileKey: string) {
    //1 obtain the pdf -> downlaod and read from pdf
    console.log('downlaodin s3 into file system')
    const file_name = await downloadFromS3(fileKey);
    if (!file_name) {
        throw new Error('could not read from S3')
    }
    const loader = new PDFLoader(file_name);
    const pages = (await loader.load()) as PDFPage[];

    for (const page of pages) {
        page.metadata.filekey = convertToAscii(fileKey);
    }

    //2. split and segment the pdf into smaller documents
    const documents = await Promise.all(pages.map(prepareDocument))

    // //3 vectorise and embed individual documents
    const vectors = await Promise.all(documents.flat().map(embedDocuments))
    console.log(vectors)
    //4 upload to pinecone
    const client = await getPineconeClient()
    const pineconeIndex = await client.Index('chatpdf')
    console.log('inserting vectors into pinecone')

    const sliceIntoChunks = (arr: PineconeRecord[], chunkSize: number) => {
        const res = [];
        for (let i = 0; i < arr.length; i += chunkSize) {
          const chunk = arr.slice(i, i + chunkSize);
          res.push(chunk);
        }
        return res;
      };

    const chunks = sliceIntoChunks(vectors, 100);



    //PineconeUtils.chunkedUpsert(pineconeIndex,vectors,namespace,10);
    var idx=0;
    for(var val of chunks){
    console.log( `upserting chunk ${idx}/${chunks.length}  -> `);
    idx= idx+1;
    pineconeIndex.upsert(val);}


    return documents[0];



}
async function embedDocuments(doc: Document) {
    try {
        const embeddings = await getEmbeddings(doc.pageContent)
        const hash = md5(doc.pageContent)
        return {
            id: hash,
            values: embeddings,
            metadata: {
                text: doc.metadata.text,
                pageNumber: doc.metadata.pageNumber,
                filekey: doc.metadata.filekey
            }
        } as PineconeRecord
    } catch (error) {
        console.log('error embedding document', error)
        throw error
    }
}

export const truncateStringbyBytes = (str: string, bytes: number) => {
    const enc = new TextEncoder()
    return new TextDecoder("utf-8").decode(enc.encode(str).slice(0, bytes))
}

async function prepareDocument(page: PDFPage) {
    let { pageContent, metadata } = page;
    pageContent = pageContent.replace(/\n/g, "");
    //split docs
    const splitter = new RecursiveCharacterTextSplitter()
    const docs = await splitter.splitDocuments([
        new Document({
            pageContent,
            metadata: {
                pageNumber: metadata.loc.pageNumber,
                text: truncateStringbyBytes(pageContent, 36000),
                filekey:metadata.filekey
            }
        })

    ])
    return docs

}