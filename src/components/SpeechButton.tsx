import { Message } from 'ai/react'
import { Speech } from 'lucide-react'
import React, { useState } from 'react'
import { Button } from './ui/button'

type Props = { messages: Message[] }



const SpeechButton = ({ messages }: Props) => {
    const [ourText, setOurText] = useState("")
    // const msg = new SpeechSynthesisUtterance()
    setOurText(String(messages[messages.length - 1]));

    const speechHandler = () => {
        // let msg
        // msg.text = ourText
        // window.speechSynthesis.speak(msg)
    }
    return (
        <div>
            <Button onClick={() => speechHandler()} className='bg-blue-900 ml-2'>
                <Speech className='h-4 w-4'></Speech>
            </Button>
        </div>
    )
}

export default SpeechButton